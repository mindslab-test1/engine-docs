---
layout: default
title: STEP1 | STF(Speech-To-Face) 전처리하기
nav_order: 2
---

# STF 데이터 전처리하기
{: .no_toc }


촬영한 데이터를 STF 학습에 사용할 수 있도록 전처리하는 방법입니다.
{: .fs-6 .fw-300 }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

전처리 과정 중 관련 오류 발생 시, <a href="https://pms.maum.ai/jira/projects/D695" target="_blank">JIRA Issue</a>를 생성해주시기 바랍니다.

## Docker Image

### 사내 docker registry 사용하기

사내 docker registry에서 아래 `tag`에 해당하는 이미지를 찾아 pull합니다.

```bash
docker pull ${사내 DOCKER REGISTRY 주소}/brain/lipsync_preprocess:v1.1.6
```

## Docker 실행하기

Docker image를 가지고 container를 생성 및 실행하는 방법은 크게 두 가지가 있습니다. 하나는 `docker-compose` 를 사용하는 것이고, 다른 하나는 `docker run` command로 직접 실행하는 방식입니다.

### docker run command

```bash
 docker run -it --ipc=host --gpus='"device=${GPU 번호}"' \
 -v ${YOUR_DATA_PATH}:${YOUR_DATA_PATH} --name ${CONTAINER 이름} \
 ${사내 DOCKER REGISTRY 주소}/brain/lipsync_preprocess:v1.1.6
```

### docker-compose

일부 서버의 경우, 서버 상 docker-compose가 설치되어 있지 않을 수 있습니다. 해당 내용을 미리 확인하시기 바랍니다.  
`docker-compose.yml` 파일 생성 후 아래 yaml 내용을 입력하시기 바랍니다.

#### example docker-compose

```yaml
version: "2.3"

services:
  lipsync_preprocess:
    image: ${사내 DOCKER REGISTRY 주소}/brain/lipsync_preprocess:v1.1.6
    environment:
      - "LC_ALL=C.UTF-8"
      - "NVIDIA_VISIBLE_DEVICES=${GPU 번호}"
    runtime: nvidia
    ipc: host

    volumes:
      - "${YOUR_DATA_PATH}:${YOUR_DATA_PATH}"
```

이후 아래의 command로 실행할 수 있습니다.

```bash
docker-compose -f ./docker-compose.yml run --name ${DOCKER CONTAINER 이름} lipsync_preprocess bash
```

## 전처리 순서

### 촬영한 영상 적재하기

촬영한 영상들을 `${YOUR_DATA_PATH}/speaker_id/version/original`안에 위치시킵니다.
  - `${YOUR_DATA_PATH}` 에는 `speaker_id`와 `version`이 디렉토리로 구분될 수 있도록 구성합니다.

  `speaker_id`
  : 사람 별 디렉토리
  (ex: `sora`, `pcm`, `teacher_woman`, ...)  
    
  `version`
  : 동일 사람에 대해 다른 데이터 분류  
  (ex: `sora_02`, `sora_03`은 `sora/02`, `sora/03`에 각각 격납)

### VAD(Voice Activity Detection) 진행하기

`vad.py`를 이용해 영상 내 녹음된 음성을 기준으로 1차 영상 분할 작업을 진행합니다.  
본 작업이 진행되고 나면, `${YOUR_DATA_PATH}/speaker_id/version/splitted` 디렉토리 내 분할된 영상이 저장되어 있어야 합니다.
  - vad detection에 사용할 오디오를 추출합니다.
  ```bash
  python vad.py --mode extract_audio \
  --video_path ${YOUR_DATA_PATH}/speaker_id/version/original \
  --wav_path ${YOUR_DATA_PATH}/speaker_id/version/vad_wav \
  --workers 1
  ```

  - vad detection을 진행합니다.
  ```bash
  python vad.py --mode vad_detection \
  --wav_path ${YOUR_DATA_PATH}/speaker_id/version/vad_wav \
  --vad_path ${YOUR_DATA_PATH}/speaker_id/version/vad_result
  ```

  - vad detection 결과를 이용해 비디오를 분할합니다.
  ```bash
  python vad.py --mode split \
  --video_path ${YOUR_DATA_PATH}/speaker_id/version/original \
  --vad_path ${YOUR_DATA_PATH}/speaker_id/version/vad_result \
  --workers 1
  ```

### 분할된 영상 검수

분할된 영상을 직접 확인하여 사용할 수 없는 데이터가 있는 지 확인합니다.  
  - 사용할 수 없는 영상을 제거할 때에는 **`remove_idx.txt`라는 파일을 생성하여 어떤 영상을 학습에서 제외시켜야 하는지** 등의 정보를 기록해둡니다.
    - 만약 영상들 중 `02_Basic_00000.mp4`, `02_Basic_00004.mp4`, `02_Basic_00006.mp4` 등의 파일이 사용할 수 없을 경우, 아래와 같이 `remove_idx.txt`에 기록합니다.
    ```
    02_Basic_00000
    02_Basic_00004
    02_Basic_00006
    ```
    
  - 기존 데이터의 영상 검수 결과 및 검수 포인트와 관련하여 [사내 Confluence 내 **전사 LipSync Data** 공간](https://pms.maum.ai/confluence/display/LD)에서 참고하실 수 있습니다.
  - 일반적으로 통제된 환경에서 촬영된 영상의 경우, 분할한 영상 중 20% 가량의 영상이 학습에서 제외됩니다.


### Resample 및 특징값 추출
분할된 영상에 대해 학습에 사용하기 용이하도록 파일 포맷 변경 및 facial landmark 추출 작업을 진행합니다.
  - 일반적으로 아래와 같이 command를 입력할 수 있습니다.
  ```bash
  python preprocess_video_training.py \
  --dir_speaker ${YOUR_DATA_PATH}/speaker_id/version \
  --workers 8 \
  --resample --audio --image --landmark
  ```
  
  `--dir_speaker`
  : `speaker_id`와 `version`을 포함한 데이터 경로를 입력합니다.
  
  `--workers`
  : 멀티프로레싱을 위한 `worker` 수를 결정합니다.  
  GPU 자원을 사용할 경우 메모리 부족이 발생할 수 있는데, 이 때 이 숫자를 줄이면 됩니다.
  
  - 각 세부 process에 대해서 진행하고 싶은 process에 대한 args를 주면 됩니다.  
  **일반적으로 학습 데이터 전처리 시 아래 4개의 옵션을 모두 사용하는 것을 권장합니다.**
  
  `--resample`
  : `vad.py`로 분할된 영상에 대해 FPS를 통일시킵니다.
  
  `--audio`
  : 영상으로부터 소리를 추출하여, 데이터 경로 하위 `tts-audio` 디렉토리 내  `.wav` 파일로 분리해냅니다.
  
  `--image`
  : 영상 내 이미지 프레임을 분할하여, 데이터 경로 하위 `image` 디렉토리 내 `.png`로 저장합니다.
  
  `--landmark`
  : facial landmark를 추출하여, 데이터 경로 하위 `landmark` 디렉토리 내 `.pkl`에 저장합니다.

## 데이터 포맷 및 형태 정리

전처리 스크립트를 구성하면서 가정한 입력 데이터의 포맷 및 형태입니다.

### 원본 비디오/오디오 등 촬영 원본 데이터

원본 데이터에 대해 가정한 spec을 정리해 두었습니다.

원본 Video Stream
: `.mp4` 확장자로 저장된 영상으로  
해상도는 `FHD` 이상, FPS는 `30` 이상 이어야 합니다.

원본 Audio Stream
: Sampling Rate가 `44kHz` 이상이어야 합니다.

분할된 비디오
: `ffmpeg -crf=12, fps=25, video idx=05d(starts from 00000), format=mp4` 로 처리됩니다.

