---
layout: default
title: STEP2 | STF(Speech-To-Face) 학습하기
nav_order: 3
---

# STF 학습하기
{: .no_toc }


STF 학습 Docker를 사용하는 방법입니다.
{: .fs-6 .fw-300 }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

학습 과정 중 궁금한 내용이 있을 경우, [JIRA Issue](https://pms.maum.ai/jira/projects/D695)를 생성해주시기 바랍니다.

`${}`로 쓰인 변수들에 대한 상세 정보는 [Variables](./#variables) 절에서 확인하실 수 있습니다.

## Docker Image

### 사내 Docker Registry 사용하기

사내 Docker Registry에서 `brain/stf`의 **최신** 이미지를 찾아 pull합니다.

```bash
docker pull ${사내 Docker Registry 주소}/brain/stf:v1.2.0
```

## Docker 실행하기

Docker image를 가지고 container를 생성 및 실행하는 방법은 크게 두 가지가 있습니다. 하나는 `docker-compose` 를 사용하는 것이고, 다른 하나는 `docker run` command로 직접 실행하는 방식입니다.

### docker run command

```bash
docker run -it --ipc=host --gpus='"device=${GPU 번호}"'  \
-v ${YOUR_DATA_PATH}:${YOUR_DATA_PATH} -p ${YOUR_PORT}:30000 --name ${CONTAINER 이름} \
${사내 DOCKER REGISTRY 주소}/brain/stf:v1.2.0
```

### docker-compose

일부 서버의 경우, 서버 상 docker-compose가 설치되어 있지 않을 수 있습니다. 해당 내용을 미리 확인하시기 바랍니다.  
`docker-compose.yml` 파일 생성 후 아래 내용을 입력하시기 바랍니다.

#### example docker-compose
```yaml
version: "2.3"

services:
  stf_train:
  	image: ${사내 DOCKER REGISTRY 주소}/brain/stf:v1.2.0
    environment:
      - "LC_ALL=C.UTF-8"
      - "NVIDIA_VISIBLE_DEVICES=${GPU 번호}"
    runtime: nvidia
    ipc: host

    volumes:
      - "${YOUR_DATA_PATH}:${YOUR_DATA_PATH}"
```

이후 아래의 command로 실행할 수 있습니다.

```bash
docker-compose -f ./docker-compose.yml run --name ${DOCKER CONTAINER 이름} stf_train bash
```

## 학습하기

STF의 학습 코드는 기본적으로 PyTorch Lightning으로 구성되어 있습니다.

### 학습 설정 수정하기

준비한 데이터에 맞추어 경로, 학습 이름 등의 설정을 수정합니다. 일반적으로 `log_dir`, `seed`, `데이터 경로`, `meta 파일 경로`를 수정하면 됩니다. 기타 학습 시 필요한 config 세부 사항은 `train_default.yaml`에서 관리되고 있습니다.

기본적인 학습의 경우 [`config/train_wav2lip.yaml`](https://pms.maum.ai/bitbucket/projects/BRAIN/repos/brain_wav2lip/browse/config/train_wav2lip.yaml)의 설정을 수정하면 됩니다.

```yaml 
logging:
  log_dir: ${YOUR_LOG_PATH}/speaker_id/version/
  seed: ${YOUR_SEED}
  nepochs: 10000

  freq:
    eval: 300 # step

######################
training:
  start_Dframe:
    tag: apply #train or apply
    crit: step # step or L1
    step: 60000
    L1: 0.000

models:
  g:
    schd:
      use: True # TODO: Can use other schedulers. Default is MultiStepLR
      milestone:
        - 10000
        - 30000
      gamma: 0.1

datasets:
  train:
    # base options
    dataset: CustomWav2LipDataset
    path_root: ${YOUR_DATA_PATH}
    id: speaker_id/version
    mode: train
    path_meta: meta/remove_idx.txt # 
    include: False

    #Augmentation
    colorjitter:
      use: True
      brightness: 0.2
      contrast: 0.2
      saturation: 0.1
    noise_factor: 2e-4
    mel_masking:
      use: False
      num_freq_masks: 2
      range_freq_mask: 27
      num_time_masks: 2
      range_time_mask: 16
    rotation:
      use: False
      min_radian: -5
      max_radian: 5
    bbox_padding:
      use: True
      min_ratio: -0.05
      max_ratio: 0.1
    bbox_translation:
      use: True
      x_value:
        min: -0.15
        max: 0.15
      y_value:
        min: -0.15
        max: 0.05

    # daaloader options
    batch_size: 4
    shuffle: True
    num_workers: 4

  test:
    # base options
    dataset: CustomWav2LipDataset
    path_root: ${YOUR_DATA_PATH}
    id: speaker_id/version
    mode: test
    path_meta: meta/remove_idx.txt # 
    include: False

    # dataloader options
    batch_size: 1
    shuffle: False
    num_workers: 1
```

만약 학습 logging을 더 자주 하고 싶다면, `logging.freq.eval` 값을 줄이면 됩니다.

만약 validation 로그를 더 자주 보고 싶다면, 다른 config인 [`config/trainer_config.yaml`](https://pms.maum.ai/bitbucket/projects/BRAIN/repos/brain_wav2lip/browse/config/trainer_config.yaml)에서 `check_val_every_n_epoch` 값을 줄이면 됩니다.

만약 checkpoint 저장을 더 자주 하게 하고 싶다면, 다른 config인 [`config/trainer_config.yaml`](https://pms.maum.ai/bitbucket/projects/BRAIN/repos/brain_wav2lip/browse/config/trainer_config.yaml)에서 `every_n_epochs` 값을 줄이면 됩니다.

### 학습 시작하기

```bash
bash pl_train.sh
```

### Logs

STF는 학습을 Tensorboard를 통해 기록하고 있습니다. 

```bash
tensorboard --logdir ${YOUR_LOG_PATH}/speaker_id/version/tensorboard --port ${YOUR_PORT}
```

## Inference

### Inference 설정 수정하기

```yaml
datasets:
  inference:
    path:
      image: ${YOUR_INFERENCE_DATA_PATH}/IMAGE/ACTION
      landmark: ${YOUR_INFERENCE_DATA_PATH}/LANDMARK/ACTION
      full_image: ${YOUR_INFERENCE_DATA_PATH}/IMAGE/ACTION
      mask: ~

models:
  g:
    ckpt: ${YOUR_CKPK_PATH}
```

### 학습 확인하기

Inference 설정을 위와 같이 수정한 후 `wav2lip_inference.sh` 의 내용을 수정합니다.

```bash
python -m wav2lip_inference\
 --config config/test_wav2lip.yaml\
 --path_background white.png\
 --video_width 1080\
 --video_height 1920\
 --gpu_ids 0\
 --path_audio ${YOUR_AUDIO_PATH}\
 --path_save temp_result
```

위와 같이 수정한 후,

```bash
bash wav2lip_inference.sh
```

를 실행하면 `./temp_result` 에 결과가 저장됩니다.

## Variables

`${사내 Docker Registry 주소}`: 사내 Docker Registry 주소.

`${GPU 번호}`: 훈련에 사용할 GPU들의 번호. `0,1`과 같이 `,`로 분리함.

`${YOUR_DATA_PATH}`: 학습용 데이터를 서버에 격납한 경로.

`${YOUR_PORT}`: 학습 로그를 확인하기 위해 개방할 포트 번호. <!-- TODO add more description(like do not use 22, 80, ...)-->

`${CONTAINER 이름}`:학습용 컨테이너에 붙일 이름. <!-- 실행한 사람 이름을 명시하도록 하기 -->

`${YOUR_LOG_PATH}`: 학습 로그를 저장할 경로. <!-- DATA1등에 저장하기 -->

`${YOUR_SEED}`: 학습 시도마다 붙일 시드. <!-- seed? -->

`${YOUR_INFERENCE_DATA_PATH}`: 테스트할 데이터가 저장되어 있는 경로.

`${YOUR_CKPT_PATH}`: checkpoint가 저장되어 있는 경로. 

`${YOUR_AUDIO_PATH}`: Inference 진행할 음성이 저장되어 있는 경로.