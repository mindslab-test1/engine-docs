---
layout: default
title: STEP3 | STF(Speech-To-Face) 서비스
nav_order: 4
---

# STF 서버 실행
{: .no_toc }


STF 서버 docker를 사용하는 방법입니다.
{: .fs-6 .fw-300 }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

서버 실행 과정 중 궁금한 내용이 있으시거나 서버 실행 관련 오류 발생 시, <a href="https://pms.maum.ai/jira/projects/S096" target="_blank">Engine API Publish로 JIRA Issue를 생성</a>해주시기 바랍니다.

## Requirements

서버 실행을 위해서는 아래와 같은 파일 및 요소들이 필요합니다.

- [STF 학습하기](../train)를 따라 학습
    - [ ] 학습 checkpoint
- [STF 데이터 전처리](../preprocess)를 통해 추출
    - [ ] 얼굴 프레임
    - [ ] 얼굴 landmark
    - [ ] background mask

wav2lip을 포함하여 여러 엔진들이 [gRPC protocol](https://grpc.io/)로 서비스를 진행하도록 구성되어 있습니다. 서버를 실행하고, gRPC 테스트를 진행할 수 있는 환경을 미리 구축해놓는 것이 좋습니다.

## Dockerfile

서비스용 docker image는 최대한 학습 docker image와 동일한 set-up을 유지하도록 합니다. 학습 docker image에는 실제 서비스 상 필요 없는 set-up이 포함되어 있을 수는 있으나, 버전 관리를 용이하게 하고 결과값이 실제 학습 docker에서 inference 했을 때와 동일하게 할 수 있습니다.

아래는 ENTRYPOINT를 포함한 sample `Dockerfile`입니다. `Dockerfile`을 사용하여 image를 직접 build하는 일은 없겠지만, 이를 참고하면 docker image 내 포함된 layer를 확인하는 데에 도움이 될 수 있습니다. 자세한 내용은 사내 docker registry에서도 확인할 수 있습니다.

```docker
FROM ${사내 DOCKER REGISTRY 주소}/brain/stf:v1.2.0

...
ENV PORT 45101
ENV OPT_PATH config/run_server.yaml
ENV SERVER_DIR /resources
ENV LOG_LEVEL INFO

ENTRYPOINT python server.py --port $PORT --config $OPT_PATH --server_dir $SERVER_DIR --log_level $LOG_LEVEL
```

## Docker Image

### 사내 docker registry 사용하기

**STF 엔진**은 CUDA 11.0 버전, CUDA 10.1 버전 두 가지로 제공됩니다.

이는 현재 사내 On-premise GPU 서버 간 Graphic Driver 버전이 통일되지 않은 서버가 존재할 수 있기 때문입니다. 참고로 서버에 설치된 CUDA 버전과 docker image 내 CUDA 버전은 일반적으로 무관합니다.  

서버 내 NVIDIA Graphic Driver 버전을 확인하기 위해 아래 command를 입력해주세요.  

```bash
nvidia-smi  # table 첫 줄 Driver Version 옆에 표시되는 version 확인 (ex. 470.57.02)
```

[`CUDA Compatibility`](https://docs.nvidia.com/deploy/cuda-compatibility/) 를 참고하여 CUDA 버전 호환성을 확인합니다.  
서버 실행을 위해서는 사내 docker registry에서 아래의 이미지 중 서버 사양에 맞게끔 pull 하시면 됩니다.  

### CUDA 11.0 (`>= 450.36.06`)

아래의 GPU가 탑재된 서버에서는 CUDA 11.0 버전 이미지를 사용해야 합니다.  

- NVIDIA A100 GPU
- GeForce RTX 30 Series
    - GeForce RTX 3080
    - GeForce RTX 3090

아래의 GPU가 탑재된 서버에서는 NVIDIA Graphic Driver 버전을 필히 확인하고, CUDA 11.0 버전 이미지 사용 여부를 결정해야 합니다.  

- NVIDIA V100 GPU
- GeForce RTX 20 Series
    - GeForce RTX 2080 Ti

사내 docker registry에서 아래 `tag`에 해당하는 이미지를 찾아 pull을 진행합니다.

```bash
docker pull ${사내 DOCKER REGISTRY 주소}/brain/wav2lip:v1.2.0.s1
```

### CUDA 10.1 (`>= 418.39`)

일반적으로 아래의 GPU가 탑재된 서버에서는 CUDA 10.1 버전 이미지를 사용할 수 있습니다.

- NVIDIA V100 GPU
- GeForce RTX 20 Series
    - GeForce RTX 2080 Ti

사내 docker registry에서 아래 `tag`에 해당하는 이미지를 찾아 pull을 진행합니다.

```bash
docker pull ${사내 DOCKER REGISTRY 주소}/brain/wav2lip:v1.2.0.s1-cu101
```

## 서버 실행 Config 파일

[`run_server.yaml`](https://pms.maum.ai/bitbucket/projects/BRAIN/repos/brain_wav2lip/browse/config/run_server.yaml?at=canary) 에 해당하는 template config는 아래와 같습니다.  
기본적으로 실험 세팅과 관련한 configuration은 [`default.yaml`](https://pms.maum.ai/bitbucket/projects/BRAIN/repos/brain_wav2lip/browse/config/default.yaml?at=refs%2Fheads%2Fcanary) 로 지정되어 docker image 내에 저장되어 있습니다.  

일반적으로는 [`run_server.yaml`](https://pms.maum.ai/bitbucket/projects/BRAIN/repos/brain_wav2lip/browse/config/run_server.yaml?at=canary) 상에 나타나는 config를 수정하는 것으로 충분합니다.

### `run_server.yaml` 예시

본 config 파일은 [`v1.2.0.s1`](https://pms.maum.ai/bitbucket/projects/BRAIN/repos/brain_wav2lip/browse?at=refs%2Ftags%2Fv1.2.0.s1) 을 기준으로 하며, [사내 bitbucket](https://pms.maum.ai/bitbucket/projects/BRAIN) 에서 직접 확인하실 수 있습니다.

#### example service config

```yaml
datasets:
  inference:
    path:
      image: /PATH/DATA/ID/IMAGE/ACTION
      landmark: /PATH/DATA/ID/LANDMARK/ACTION
      full_image: /PATH/DATA/ID/IMAGE/ACTION
      mask: /PATH/DATA/ID/MASK/ACTION
      webm: /PATH/DATA/ID/WEBM.webm

    chunk_size: 1

    imsize: 512
    fps: 25

    jaw_factor: 0.25
    padding:
      tail: 2 # seconds
      full_video_as_inference: True
      drop_last_segment: True

    num_workers: 1

    video_loop:
      start_index: ~ # same with len(action_video)
      add_reverse_video: False

    crf: 12
    max_video_frame: 250 # recommend multiple of the frame number of background video

models:
  g:
    model: Wav2LipHalfMaskModel
    ckpt: "/PATH/CHECKPOINT.ckpt"

audio_params: config/audio_param_22k.yaml

postprocess_size: 16
use_nvenc: False # False if A100 GPU else True
extract_webm: False

```

## Docker 실행하기

현재 코드에서는 `docker-compose.yml` 을 제공하고 있으며, [`brain_vision_engines`](https://pms.maum.ai/bitbucket/projects/BRAIN/repos/brain_vision_engines/browse)에서도 확인할 수 있습니다.

### docker run command

```bash
 docker run -itd --ipc=host --network="bridge" --gpus='"device=${GPU 번호}"' \
 -v ${행동영상경로}:/resources -v ${서버 config 경로}:/root/wav2lip/config/run_server.yaml \
 -p ${사용할 PORT}:45101 --name ${CONTAINER 이름} \
 ${사내 DOCKER REGISTRY 주소}/brain/wav2lip:v1.2.0.s1
```

### docker-compose

일부 서버의 경우, 서버 상 docker-compose가 설치되어 있지 않을 수 있습니다. 해당 내용을 미리 확인하시기 바랍니다.  
`docker-compose.yml` 파일 생성 후 아래 yaml 내용을 저장하시기 바랍니다.

#### example docker-compose

```yaml
version: "2.3"
 
services:
  ${CONTAINER 이름}:
    image: ${사내 DOCKER REGISTRY 주소}/brain/wav2lip:v1.2.0.s1
    environment:
      - "LC_ALL=C.UTF-8"
      - "NVIDIA_VISIBLE_DEVICES=${GPU 번호}"
    runtime: nvidia
    ipc: host
    ports:
      - "${사용할 PORT}:45101"
    network_mode: bridge
    volumes:
      - "${행동영상경로}:/resources"
      - "${서버 config 경로}:/root/wav2lip/config/run_server.yaml"
```

이후 아래의 command로 실행할 수 있습니다.

```bash
docker-compose -f ./docker-compose.yml up -d
```